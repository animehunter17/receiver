<?php
namespace tests;

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

final class AlexaTest extends TestCase
{
    public function testGetMediaOnDeck()
    {
        // echo $response->getStatusCode();
        $client = new Client(['base_uri' => 'http://localhost:2020/']);
        $response = $client->request('GET', 'Alexa/getMediaOnDeck');

        $contents = (string) $response->getBody();
        // echo $contents;
        // print_r($response->getHeader('content-type'));
        $headers = $response->getHeader('content-type');
        $body = json_decode($contents, true);

        $this->assertEquals($headers[0], "application/json; charset=utf-8");
        $this->assertArrayHasKey('data', $body);
        $this->assertArrayHasKey('code', $body);
        $this->assertArrayHasKey('message', $body);

        $data = $body['data'][0];

        $this->assertArrayHasKey('key', $data);
        $this->assertArrayHasKey('MediaPart', $data);
        $this->assertArrayHasKey('Token', $data);
        $this->assertArrayHasKey('title', $data);
        $this->assertArrayHasKey('type', $data);
        $this->assertArrayHasKey('year', $data);
        $this->assertArrayHasKey('thumb', $data);
        $this->assertArrayHasKey('duration', $data);
        $this->assertArrayHasKey('addedAt', $data);
        $this->assertArrayHasKey('grandparentTitle', $data);

    }

    public function testGetRecentAddedMedia()
    {
        $client = new Client(['base_uri' => 'http://localhost:2020/']);
        // $response = $client->request('GET', 'Alexa/getRecentAddedMedia');
        $response = $client->request('POST', 'Alexa/getRecentAddedMedia', []);

        $contents = (string) $response->getBody();
        // echo $contents;
        // print_r($response->getHeader('content-type'));
        $headers = $response->getHeader('content-type');
        $body = json_decode($contents, true);

        // print_R($headers);
        $this->assertEquals($headers[0], "application/json; charset=utf-8");
        $this->assertArrayHasKey('data', $body);
        $this->assertArrayHasKey('code', $body);
        $this->assertArrayHasKey('message', $body);


    }

}
