<?php
namespace System;

class Request
{
    private $request;

    public function __construct()
    {
        //GET RAW Post data from CURL Request
        $post = file_get_contents("php://input");
        parse_str($post, $this->request);
    }

    public function getRequest()
    {
        return new self();
    }

    public function get($input)
    {
        if (isset($_GET[$input]) && is_string($_GET[$input])) {
            return trim(filter_input(INPUT_GET, $input, FILTER_SANITIZE_STRING));
        }

        if (isset($_GET[$input]) && is_numeric($_GET[$input])) {
            return trim(filter_input(INPUT_GET, $input, FILTER_SANITIZE_NUMBER_INT));
        }

        return false;
    }

    public function post($input)
    {
        if (isset($this->request[$input]) && is_string($this->request[$input])) {
            return trim(filter_var($this->request[$input], FILTER_SANITIZE_STRING));
        }

        if (isset($this->request[$input]) && is_numeric($this->request[$input])) {
            return trim(filter_var($this->request[$input], FILTER_SANITIZE_NUMBER_INT));
        }

        return false;
    }

}
