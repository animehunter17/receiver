<?php
namespace System;

class Router
{
    private $routes;

    const DEFAULT_URI = "/";
    const DEFAULT_ROUTE = "Home@Index";

    public function __construct()
    {
        $this->routes = include dirname(__DIR__)."/src/routes.php";
    }

    public function Index()
    {
        $req_method = strtolower($_SERVER['REQUEST_METHOD']);
        $request_uri = $_SERVER['REQUEST_URI'];
        error_log("req uri: ".$request_uri);
        error_log("GET: ".var_export($_GET, true));

        // if (!empty($_GET)) {
        //     $query_part = strstr($request_uri, "?", true);
        //     error_log("query part: ".var_export($query_part, true));
        // }

        //check routes
        if (isset($this->routes[$req_method]) && !empty($this->routes[$req_method])){
            //loop through the defined routes
            foreach($this->routes[$req_method] as $uri => $route) {
                // if ($req_method == "get" && strpos($uri, "?(:any") !== false) {
                //if request is GET then get only uri upto ?
                if ($req_method == "get") {
                    if (strpos($request_uri, "?") !== false) {
                        // error_log("get request");
                        $request_uri = strstr($request_uri, "?", true);
                        // error_log("request uri: ".$request_uri);
                    // $route_part = strstr($uri, "?", true);
                    // if ($route_part && isset($query_part)) {
                    //     $req_route = $route;
                    //     break;
                    }
                }
                //hit first matching route then break loop
                if ($request_uri == $uri){
                    $req_route = $route;
                    break;
                }
            }
        }

        //check if request uri is default uri
        if ($request_uri == self::DEFAULT_URI){
            $req_route = self::DEFAULT_ROUTE;
        }

        //check if req_route is set
        if (!isset($req_route)){
            echo "No Route!";
            exit;
        }

        //begin to call the class and method
        try {
            $parts = explode("@", $req_route);
            $controller = ucfirst($parts[0]);
            $class = "\App\controllers\\$controller";
            $myclass = new $class;

            if ($req_method == "post") {
                $myclass->{$parts[1]}(new Request);
            } else {
                $myclass->{$parts[1]}();
            }
        } catch(\Exception $e) {
            echo $e->getMessage();

        }



    }

}
