<?php


function __autoload($class){

	if(file_exists("src/controllers/".ucfirst($class).".php")) {
        require_once "src/controllers/".ucfirst($class).".php";
        return;
    }

	if(file_exists("src/config/".ucfirst($class).".php")) {
        require_once "src/config/".ucfirst($class).".php";
        return;
    }

	if(file_exists("src/libraries/".lcfirst($class).".php")) {
        require_once "src/libraries/".lcfirst($class).".php";
        return;
    }
	
	if(file_exists("src/models/".lcfirst($class).".php")) {
        require_once "src/views/".lcfirst($class).".php";
        return;
    }

}
