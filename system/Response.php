<?php
namespace System;

use System\Logger;

class Response
{
    const SUCCESS = 200;
    const NO_CONTENT = 204;

    const BAD_REQUEST = 400;
    const FORBIDDEN = 403;
    const SERVER_ERROR = 500;

	public static function sendResponse($response=false)
    {
		if(!$response) {
			$response = array("code" => self::BAD_REQUEST, "message" => "Bad Request");
        }

        if (isset($response['code'])) {
            switch($response['code']) {
                case self::SUCCESS:
                    $response['message'] = "success";
                    break;
                case self::NO_CONTENT:
                    $response['message'] = "Empty result";
                    break;
                case self::BAD_REQUEST:
                    $response['message'] = "Bad request";
                    break;
                case self::FORBIDDEN:
                    $response['message'] = "Forbidden request";
                    break;
                default:
                    $response['message'] = "Server error";
            }
        }

		if (is_array($response)) {
			$response = json_encode($response);
        }

error_log("response: ".var_export($response, true));
        // header("Access-Control-Allow-Origin: *");
        // header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
        header("Content-Type: application/json; charset=utf-8");
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
		echo $response;	
        // Logger::info("resp :".var_Export($response, true));
        // return;
        exit;
		
	}



}
