<?php
namespace System;

class Db 
{
    private $where;
    private $limit;
    private $join;
    private $order;
    // private $query;
    // private $connection;


    protected static $instance = null;

    private function __construct()
    {
        $this->connect();	
    }

    public static function Instance()
    {
        if(is_null(self::$instance)){
            self::$instance = new self();
        }

        return self::$instance;

    }


    private function connect()
    {
        $username = Ini::getConfig("database.mysql_username");
        $pwd = Ini::getConfig("database.mysql_password");
        $host = Ini::getConfig("database.mysql_host");
        $dbname = Ini::getConfig("database.mysql_database");

        try {
            $pdo = new \PDO("mysql:host=$host; dbname=$dbname", $username, $pwd);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {
            echo "cannot connect: ".$e->getMessage();
        }
    }


}
