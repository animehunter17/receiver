<?php
namespace System;

class View {
 
    public function load($view, $data = array())
    {
        if(file_exists("src/views/".$view.".php")) {
            //extract data array of values if not empty
            if(!empty($data))
            extract($data);
            require_once("src/views/".$view.".php");

        }

    }


}
