<?php
namespace System;

use System\Library;
use System\View;
use System\Response;

class BaseController 
{
	protected static $request;
    
    public static $model_name;

    public function __construct()
    {
		$this->library = new Library;
		$this->view = new View;
        $this->_checkRequest();
    }

	public static function filterVar($input)
    {
		if (is_string($input)) {
			return trim(filter_var($input, FILTER_SANITIZE_STRING));
        }

		if (is_numeric($input)) {
			return trim(filter_var($input, FILTER_SANITIZE_NUMBER_INT));
        }

        // throw new \Exception("There are empty values");
        return false;
	}

	protected function _checkRequest()
    {
        if ($_SERVER['HTTP_HOST'] == "localhost:2020" || $_SERVER['REMOTE_ADDR']=="192.168.1.201" || strpos($_SERVER['REMOTE_ADDR'], "192.168.1") !== FALSE || $_SERVER['REMOTE_ADDR'] == "108.61.172.254") {
            return true;
        }

        //check headers and remote address
		if (($_SERVER['REMOTE_ADDR'] != "108.61.172.254" && $_SERVER['REMOTE_ADDR'] != Ini::getConfig("homeweb.External_ip")) || ($_SERVER['HTTP_CLIENT_ID'] != Ini::getConfig("homeweb.Client_id"))) {

            // error_log("headers: ".var_export(\getallheaders(), true));
            error_log("server: ".var_export($_SERVER, true));
            error_log("sending bad request");
            error_log("remote ip: ".$_SERVER['REMOTE_ADDR']);
            return Response::sendResponse();
		}

	}


}
