<?php
namespace System;

class Logger 
{

    public static function info($info)
    {
        if($info){
            $file = "/var/log/openresty/receiver/error.log";
            $date = date('Y/m/d H:i:s');
            $fp = fopen($file, "a");
            fwrite($fp, "[".$date."][INFO] - ".$info."\n");
            fclose($fp);
        }
    }

    public static function error($error)
    {
        if($error){
            // $file = "/var/log/dev/longdestiny/error.log";
            $file = "/var/log/openresty/receiver/error.log";
            $date = date('Y/m/d H:i:s');
            $fp = fopen($file, "a");
            fwrite($fp, "[".$date."][ERROR] - ".$error."\n");
            fclose($fp);
        }
    }
}

