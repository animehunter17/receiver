<?php

return [
    "get" => [
        "/plex/getRecentlyAdded" => "plex@getRecentlyAdded",
        "/plex/getAllLibraries" => "plex@getAllLibraries",
        "/plex/getMediaOnDeck" => "plex@getMediaOnDeck",
        "/plex/getAvailablePlexClients" => "plex@getAvailablePlexClients",
        "/plex/search" => "plex@search",

        "/Alexa/getMediaOnDeck" => "alexa@getMediaOnDeck",
        "/Alexa/getRecentAddedMedia" => "alexa@getRecentAddedMedia",
        "/Alexa/playPlaylist" => "alexa@playPlaylist",
    ],
    "post" => [
        "/plex/playback" => "plex@playback",
        "/plex/search" => "plex@search",
        "/plex/getMediaInLibrary" => "plex@getMediaInLibrary",
        "/plex/getEpisodesList" => "plex@getEpisodesList",
        "/plex/playMedia" => "plex@playMedia",
        "/plex/getSongList" => "plex@getSongList",
        "/plex/getAlbumSongs" => "plex@getAlbumSongs",
        
        "/Alexa/playLatestEpisode" => "alexa@playLatestEpisode",
        "/Alexa/playSpecificEpisode" => "alexa@playSpecificEpisode",
        "/Alexa/playMusicToTV" => "alexa@playMusicToTV",
        "/Alexa/getRecentAddedMedia" => "alexa@getRecentAddedMedia",

        "/cronjob/item" => "cronjob@addNewDownloadItem",
    ],
    "put" => [],
    "patch" => [],
    "delete" => [
        "/cronjob/item" => "cronjob@removeDownloadItem",
    
    ],

];
