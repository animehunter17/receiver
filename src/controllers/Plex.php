<?php
namespace App\controllers;

use System\BaseController;
use System\Logger;
use System\Response;
use App\libraries\plexLib;
use App\libraries\memcacheLib as MemcacheLib;
use App\entity\PlexEntity;

class Plex extends BaseController {

    private $result;
    private $plexEntity;

    public function __construct()
    {
        parent::__construct();
        $this->plexEntity = new PlexEntity;
        $this->plexLib = new plexLib($this->plexEntity);
        // $this->_checkRequest();
        $this->result['code'] = Response::SERVER_ERROR;
    }


    public function playback()
    {
        // error_log("request: ".var_export($data, true));
        $control = BaseController::filterVar($this->request["control"]);
        $category = BaseController::filterVar($this->request["category"]);
        $query = BaseController::filterVar($this->request["keyword"]);

        Logger::info("request: ".var_export($this->request, true));

        try {
            $response = $this->plexLib->searchMedia($query);
            // error_log("data: ".var_Export($response, true));

            $this->result['code'] = Response::SUCCESS;
            $this->result['data'] = $response;
            // $devices = $this->plexLib->getDevices("Nexus 5X");
            // $library = $this->library->plexLib->signInPlex();
            // $plexserver = $this->library->plexLib->getPlexServerDetails();
            // $library = $this->library->plexLib->getServers();
            // $library = $this->library->plexLib->getAllMediaInLibrary(4);
            // $this->library->plexLib->playMedia();
        } catch (\Exception $e){
            $this->result['message'] = $e->getMessage();
        }	

        Logger::info(__FUNCTION__." result: ".var_Export($this->result, true));
        Response::sendResponse($this->result);

    }

    /*******
     *input keyword. example - "Marvels Avengers"
     * 
     *****/
    public function search()
    {
        try {
            $query = $this->plexEntity->getKeyword();

            $this->memcache = new MemcacheLib;
            //Look for search in Memcache
            $result = $this->memcache->getItem($query);

            if (!$result){
                $this->plexLib->searchMedia($query);
                $result = $this->plexEntity->getData();
                //store in memcache for 20mins
            	$this->memcache->addItem($query, $result, 1200);
            }
            $this->result = $this->plexEntity->getResult();

        } catch (\Exception $e) {

            $this->result['message'] = $e->getMessage();

        }

        Response::sendResponse($this->result);

    }

    public function __getDetails()
    {
        Logger::info("getting details");

        $query = BaseController::filterVar($this->request["keyword"]);

        try {
            switch($query){
            case "devices":
                $data = $this->plexLib->getDevices();
                break;
            case "servers":
                $data = $this->plexLib->getServers();
                break;	
            case "serverDetails":
                $data = $this->plexLib->getPlexServerDetails();
                break;
            default:
                $query = false;

            }

            $this->result['code'] = Response::SUCCESS;
            if (!empty($data)) {
                $this->result['data'] = $data;
            } else {
                $this->result['code'] = Response::NO_CONTENT;
            }


            if(!$query) {
                $this->result['code'] = Response::FORBIDDEN;
            }

        } catch (\Exception $e) {
            $this->result['message'] = $e->getMessage();
        }

        Response::sendResponse($this->result);

    }

    public function getRecentlyAdded()
    {
        try {
            $this->plexLib->getRecentlyAdded();
            $data = $this->plexEntity->getData();
            $this->result = $this->plexEntity->getResult();

        } catch(\Exception $e){
            $this->result['message'] = $e->getMessage();
        }

        Response::sendResponse($this->result);

    }

    public function getAllLibraries()
    {
        try {
            $this->plexLib->getLibraries();
            $library = $this->plexEntity->getData();

            $this->result = $this->plexEntity->getResult();

        } catch(\Exception $e) {

            $this->result['message'] = $e->getMessage();

        }

        Response::sendResponse($this->result);

    }

    /*******
     *input id. example - 13
     * 
     *****/
    public function getMediaInLibrary()
    {
        try {
            $keyId = $this->plexEntity->getId();
            Logger::info("get media in library: ".$keyId);
            $this->plexLib->getAllMediaInLibrary($keyId);
            $media = $this->plexEntity->getData();
            // error_log("media: ".var_export($media, true));

            $this->result = $this->plexEntity->getResult();

        } catch(\Exception $e) {

            $this->result['message'] = $e->getMessage();
        }

        Response::sendResponse($this->result);
    }

    /*******
     *input id. example - "/library/metadata/58686/children"
     * 
     *****/
    public function getEpisodesList()
    {
        try {
            $keyId = $this->plexEntity->getId();
            Logger::info("get episodes in library: ".$keyId);

            $this->plexLib->getAllEpisodes($keyId);
            $media = $this->plexEntity->getData();
            // Logger::info("media: ".var_export($media, true));
            $this->result = $this->plexEntity->getResult();

        } catch(\Exception $e) {

            $this->result['message'] = $e->getMessage();
        }

        Response::sendResponse($this->result);
    }

    public function getMediaOnDeck()
    {
        try {
            $this->plexLib->OnDeck();
            $deck = $this->plexEntity->getData();

            $this->result = $this->plexEntity->getResult();

        } catch(\Exception $e) {
            $this->result['message'] = $e->getMessage();
        }

        Response::sendResponse($this->result);

    }

    public function getAvailablePlexClients()
    {
        try {
            $this->plexLib->getAvailableClients();
            // $clients = $this->plexEntity->getData();
            // $this->result = $this->plexEntity->getResult();
            $this->result = $this->plexEntity->getClients();

        } catch(\Exception $e) {
            $this->result['message'] = $e->getMessage();
        }

        Response::sendResponse($this->result);

    }

    /*******
     *input mediaKey. example - "/library/metadata/58686/children"
     *input client. example - "192.168.1.20:3005"
     *****/
    public function playMedia()
    {
        try {
            $mediaKey = $this->plexEntity->getMediaKey();
            $client = $this->plexEntity->getClient();
            $this->plexLib->playMedia($client, $mediaKey);

            $this->result = $this->plexEntity->getResult();
        } catch (\Exception $e){
            $this->result['message'] = $e->getMessage();
        }	

        Logger::info(__FUNCTION__." result: ".var_Export($this->result, true));
        Response::sendResponse($this->result);

    }

    public function getSongList()
    {
        try {
            $mediaKey = $this->plexEntity->getMediaKey();
            $this->plexLib->getSongList($mediaKey);
            $response = $this->plexEntity->getData();

            $this->result = $this->plexEntity->getResult();

        } catch (\Exception $e){
            $this->result['message'] = $e->getMessage();
        }	

        Logger::info(__FUNCTION__." result: ".var_Export($this->result, true));
        Response::sendResponse($this->result);
    }

    public function getAlbumSongs()
    {
        try {
            $mediaKey = $this->plexEntity->getMediaKey();
            $response = $this->plexLib->getAlbumSongs($mediaKey);

            $this->result['code'] = Response::SUCCESS;
            $this->result['data'][] = $response;

        } catch (\Exception $e){
            $this->result['message'] = $e->getMessage();
        }	

        Logger::info(__FUNCTION__." result: ".var_Export($this->result, true));
        Response::sendResponse($this->result);

    }

 
}
