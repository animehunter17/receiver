<?php
namespace App\controllers;

use System\BaseController;
use System\Response;
use App\libraries\memcacheLib as MemcacheLib;
use App\libraries\plexLib;
use App\entity\PlexEntity;
use System\Logger;

class Alexa extends BaseController {

    private $result;
    private $plexClient;

    const LIST_SIZE = 20;
    // const CACHE_LIMIT = 1200; //20mins
    const CACHE_LIMIT = 30; 

    public function __construct()
    {
        parent::__construct();
        $this->plexEntity = new PlexEntity;
        $this->plexLib = new plexLib($this->plexEntity);
        $this->_checkRequest();
        $this->result['code'] = Response::SERVER_ERROR;
    }

    public function playMovie()
    {
        $title = self::filterVar($this->request["keyword"]);
        $result = $this->plexLib->searchMedia($title, true, false);
        // error_log("res: ".var_export($result, true));
    }

    /*******
     *input keyword. example - "Marvels Avengers"
     * 
     *****/
    public function playLatestEpisode()
    {
        $query = $this->plexEntity->getKeyword();
        $this->plexLib->searchMedia($query, false, true);
        $result = $this->plexEntity->getData();

        if (is_array($result)){
            $this->plexLib->getAllEpisodes($result[0]['key']);
            $media = $this->plexEntity->getData();
        }

        if (isset($media) && !empty($media)){
            foreach ($media[0]['episodelist'] as $video):
                // print_r($video);
                if (!$video['viewCount']){
                    // print_R($video);
                    $unwatched = $video;
                    break;
                }
            endforeach;
        }

        if (isset($unwatched)){
            // error_log("unwatch: ".var_export($unwatched, true));
            $this->plexLib->getAvailableClients(true);
            $this->plexClient = $this->plexEntity->getData();
            // print_r($this->plexClient);

            $playResult = $this->playMedia($unwatched['key']);

            if (isset($playResult['code']) && $playResult['code'] == 200){
                $this->result['code'] = Response::SUCCESS;
                $this->result['data'][] = $unwatched;
            }
        }

        Response::sendResponse($this->result);
    }

    /*******
     *input title. example - "Marvels Avengers"
     *input season. example - "1"
     *input episode. example - "2"
     * 
     *****/
    public function playSpecificEpisode()
    {
        Logger::info("playing specific episode");
        $title      = $this->plexEntity->getTitle();
        $season     = $this->plexEntity->getSeason();
        $episode    = $this->plexEntity->getEpisode();

        $media = $this->getSeasonsAndEpisodesOnly($title);
        Logger::info("media: ".var_Export($media, true));
        $found = $this->findEpisode($media, $season, $episode);
        Logger::info("found: ".var_Export($found, true));
        // error_log("media: ".var_export($media, true));
        // error_log("epside: ".var_export($found, true));
        if (is_array($found)){
            $this->plexLib->getAvailableClients(true);
            $this->plexClient = $this->plexEntity->getClients();
            Logger::info("clients: ".var_export($this->plexClient, true));
            // print_r($this->plexClient);
            $playResult = $this->playMedia($found['key']);
            Logger::info("play res: ".var_export($playResult, true));

            if (isset($playResult['code']) && $playResult['code'] == Response::SUCCESS){
                $this->result['code'] = Response::SUCCESS;
                // $this->result['data'][] = $unwatched;
            }
        }

        Response::sendResponse($this->result);
    }

    public function playMusicToTV()
    {
        try {
            $query = $this->plexEntity->getKeyword();
            $lang = $this->plexEntity->getLang();

            Logger::info("query: ".$query);
            Logger::info("lang: ".$lang);
            $result = [];

            $LibraryMusics = $this->plexLib->availableLibraryMusic();
            $strlower_query = strtolower($lang);
            $oneWordQuery = str_replace(" ", "_", $query);

            $this->memcache = new MemcacheLib;
            $cached = true;
            //language music request
            if (array_key_exists($strlower_query, $LibraryMusics)){
                $result = $this->memcache->getItem($strlower_query."_music");
                Logger::info("lang cache: ".var_export($result, true));
                if (!$result){
                    $cached = false;
                    $this->plexLib->getAllMediaInLibrary($LibraryMusics[$strlower_query]);
                    $result = $this->plexEntity->getData();
                }
            } else {
                Logger::info("getting artist");
                //specific artist request
                $result = $this->memcache->getItem($oneWordQuery."_artist");
                // Logger::info("artist cache: ".var_export($result, true));
                if (!$result){
                    $cached = false;
                    //contains artists, tracks
                    $this->plexLib->searchMedia($query, false, true);
                    $result = $this->plexEntity->getData();
                    // Logger::info("result: ".var_export($result, true));
                }
            }

            //if data is not cached then build the song list and add to cache
            // Logger::info("result: ".var_export($result, true));
            if (!$cached) {
                $result = $this->getAllAlbumsAndTracks($result);
                if ($lang) {
                    $this->memcache->addItem($strlower_query."_music", $result, self::CACHE_LIMIT);
                }

                if ($query) {
                    $this->memcache->addItem($oneWordQuery."_artist", $result, self::CACHE_LIMIT);
                }
            }
            // Logger::info("albums tracks: ".var_export($result, true));

            //randomize tracks
            // shuffle($result);

            //get first item from shuffled list
            // $shuffle_selected = array_pop($result);
            // $this->plexClient = $this->plexLib->getAvailableClients(true);

            // $playResult = $this->playMedia($shuffle_selected['key']);
            // if (isset($playResult['code']) && $playResult['code'] == 200){
            //     $this->result['data'] = $shuffle_selected;
            // }
            $this->plexEntity->setData($result);
            // $this->plexEntity->setData($shuffle_selected);
            $this->result = $this->plexEntity->getResult();
            error_log("res: ".var_Export($this->result, true));
        } catch(Exception $e) {
            error_log("error: ".var_Export($e->getMessage(), true));
        }

        Response::sendResponse($this->result);
    }

    private function playMedia($mediaKey)
    {
        if (!$this->plexClient){
            throw new Exception("No client available");
        }

        $client = "http://".$this->plexClient['host'].":".$this->plexClient['port'];
        return $this->plexLib->playMedia($client, $mediaKey);
    }

    public function getMediaOnDeck()
    {
        try {
            $this->plexLib->OnDeck();

            $this->result = $this->plexEntity->getResult();

        } catch(Exception $e) {
            $this->result['message'] = $e->getMessage();
        }
        // Logger::info("deck : ".var_export($this->result, true));

        Response::sendResponse($this->result);

    }

    private function getSeasonsAndEpisodesOnly($title)
    {
        Logger::info("searching $title");
        $this->plexLib->searchMedia($title, false, true);

        $result = $this->plexEntity->getResult();
        $media = array();

        Logger::info("res: ".var_export($result, true));
        if (is_array($result)){
            // $media = $this->plexLib->getAllEpisodes($result[0]['key']);
            $this->plexLib->getAllEpisodes($result['data'][0]['key']);
            $media = $this->plexEntity->getResult();
        }

        // Logger::info("before shift: ".var_Export($media, true));
        // array_shift($media);

        // Logger::info("shifted media: ".var_Export($media, true));
        return $media;
    }

    private function findEpisode($media, $season, $episode)
    {
        // Logger::info("media: ".var_export($media, true));
        foreach ($media['data'] as $item) :
            if ($item['type'] == "season" && strpos($item['title'], $season) !== FALSE) {
                foreach ($item['episodelist'] as $ep) :
                    if ($ep['index'] == $episode) {
                        return $ep; 
                    }
                endforeach;
            } 
        endforeach;
    }

    private function getAllAlbumsAndTracks($result)
    {
        //get album song list and remove artist type from result leaving only tracks
        $albums = array();
        if (is_array($result)){
            foreach ($result as $key => $value) :
                if ($value['type'] == "artist"){
                    $this->plexLib->getSongList($value['key']);
                    $albums[] = $this->plexEntity->getData();
                    unset($result[$key]);
                }
            endforeach;
        }

        //reorder album tracks array to get only tracks and no parent album array.
        $albumTracks = $this->getAlbumTracksOnly($albums, $result);

        $result = array_merge($result, $albumTracks);

        return $result;
    }

    private function getAlbumTracksOnly($albums, $songlist)
    {
        //Loop through albums then loop through tracklist and add to songlist array
        foreach ($albums as $item) :
            // Logger::info("item: ".var_export($item, true));
            if (isset($item[0]['tracklist'])){
                foreach ($item[0]['tracklist'] as $track) :
                    array_push($songlist, $track);
                    // $tracks[] = $item['tracklist'];
                endforeach;
            }
        endforeach;
        // Logger::info("list: ".var_export($songlist, true));

        return $songlist;
    }

    public function getRecentAddedMedia()
    {
        try {
            $query = $this->plexEntity->getKeyword();
            $recent = $this->getRecentMedia($query);

            Logger::info("recent: ".var_export($recent, true));
            $this->result = $this->plexEntity->getResult();

        } catch(Exception $e) {
            Logger::info("error: ".$e->getMessage());
        }

        Response::sendResponse($this->result);
    }

    private function getRecentMedia($query=false)
    {
        $recent = [];
        $this->memcache = new MemcacheLib;

        if (!$query) {
            $recent = $this->memcache->getItem("allRecentAdded");
            if (!$recent) {
                $this->plexLib->getRecentlyAdded(self::LIST_SIZE);
                $recent = $this->plexEntity->getData();
                $this->memcache->addItem("allRecentAdded", $recent, self::CACHE_LIMIT);
            }
        } elseif ($query == "series") {
            $recent = $this->memcache->getItem("recentAddedSeries");
            if (!$recent) {
                $this->plexLib->getRecentlyAdded(self::LIST_SIZE, false, true);
                $recent = $this->plexEntity->getData();
                $this->memcache->addItem("recentAddedSeries", $recent, self::CACHE_LIMIT);
            }
        } elseif ($query == "movies" || $query == "films") {
            $recent = $this->memcache->getItem("recentAddedMovies");
            if (!$recent) {
                $this->plexLib->getRecentlyAdded(self::LIST_SIZE, true, false);
                $recent = $this->plexEntity->getData();
                $this->memcache->addItem("recentAddedMovies", $recent, self::CACHE_LIMIT);
            }
        } elseif ($query == "music") {
            $recent = $this->memcache->getItem("recentAddedMusic");
            if (!$recent) {
                $this->plexLib->getRecentAddedMusic();
                $data = $this->plexEntity->getData();
                // Logger::info("data: ".var_Export($data, true));
                $tracks = [];
                foreach ($data as $item) :
                    foreach ($item as $track) :
                        $tracks[] = $track; 
                    endforeach;
                endforeach;
                $this->plexEntity->setData($tracks);
                $recent = $tracks;
                // Logger::info ("tracks: ".var_Export($recent, true));
                $this->memcache->addItem("recentAddedMusic", $recent, self::CACHE_LIMIT);
            }
        }

        return $recent;
    }

    public function playPlaylist()
    {
        $data = $this->plexLib->getPlaylist();
        Logger::info("playlist: ".var_export($data[2], true));
        $list = $this->plexLib->getAlbumSongs($data[2]['key']);
        Logger::info("songs: ".var_export($list, true));

        Response::sendResponse($list);
    }


}
