<?php


class Download extends BaseController {

    public function __construct()
    {
		parent::__construct();
		$this->_checkRequest();
	}

    public function torrent()
    {
		$query = BaseController::filterVar($this->request["query"]);
		$episode = BaseController::filterVar($this->request["episode"]);
		error_log("query: ".$query." episode: ".$episode);
		$result['message'] = "Successful";
		$result['code'] = 200;
		$cmd ="/usr/bin/python3 /home/ranma/asskick/incoming.py '".$query."' ".$episode;
		error_log("cmd: ".$cmd);
		if($episode)
			$command = escapeshellcmd("/usr/bin/python3 /home/ranma/asskick/incoming.py '".$query."' ".$episode);
		else
			$command = escapeshellcmd("/usr/bin/python3 /home/ranma/asskick/incoming.py '".$query."'");

		$output = shell_exec($command);
		// echo $output;

		$this->_sendReponse($result);

	}

    public function HkDramaDownload()
    {
        $query = BaseController::filterVar($this->request["query"]);
		$episode = BaseController::filterVar($this->request["episode"]);
		error_log("query: ".$query." episode: ".$episode);
		$result['message'] = "Successful";
		$result['code'] = 200;
		// $cmd ="/usr/bin/python3 /home/ranma/asskick/incoming.py '".$query."' ".$episode;
		// error_log("cmd: ".$cmd);
		if($episode) {
			$command = escapeshellcmd("/usr/bin/python3 /home/ranma/asskick/incomingHKDrama.py '".$query."' ".$episode);
        } elseif($query){
            $command = escapeshellcmd("/usr/bin/python3 /home/ranma/asskick/incomingHKDrama.py '".$query."'");
        } else {
            $command = escapeshellcmd("/usr/bin/python3 /home/ranma/asskick/incomingHKDrama.py");

        }
        error_log("running command: ".$command);

		$output = shell_exec($command);

		$this->_sendReponse($result);

    }

    public function JdramaDownload()
    {
        $query = BaseController::filterVar($this->request["query"]);
		$episode = BaseController::filterVar($this->request["episode"]);
		error_log("query: ".$query." episode: ".$episode);
		$result['message'] = "Successful";
		$result['code'] = 200;
		// $cmd ="/usr/bin/python3 /home/ranma/asskick/incoming.py '".$query."' ".$episode;
		// error_log("cmd: ".$cmd);
		if($episode) {
			$command = escapeshellcmd("/usr/bin/python3 /home/ranma/asskick/incomingJapDrama.py '".$query."' ".$episode);
        } elseif($query){
            $command = escapeshellcmd("/usr/bin/python3 /home/ranma/asskick/incomingJapDrama.py '".$query."'");
        } else {
            $command = escapeshellcmd("/usr/bin/python3 /home/ranma/asskick/incomingJapDrama.py");
        }
        error_log("running command: ".$command);

		$output = shell_exec($command);

		$this->_sendReponse($result);

    }

}
