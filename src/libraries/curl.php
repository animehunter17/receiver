<?php
namespace App\libraries;

use System\Library;
use System\Logger;

class Curl extends Library 
{
    private $ch;

    public function __construct()
    {
        // parent::__construct();
        $this->ch = curl_init();
    } 

    public function curl_url($url)
    {
        if (!empty($url) && is_string($url)){
            if (!curl_setopt($this->ch, CURLOPT_URL, $url))
                throw new Exception("cannot create curl instance");
        }
    }

    public function headers($headers)
    {
        if (is_bool($headers) && ($headers))
            curl_setopt($this->ch, CURLOPT_HEADER, $headers);
    }

    public function curl_ssl($ssl)
    {
        if (is_bool($ssl) && ($ssl))
            curl_setopt($this->ch, CURLOPT_SSL_VERIFY_PEER, $ssl);
    }

    public function curl_post($post)
    {
        if (is_bool($post))
            curl_setopt($this->ch, CURLOPT_POST, $post);
    }

    public function returnTransfer($return)
    {
        if (is_bool($return))
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, $return);
    }

    public function userPwd($userpwd)
    {
        if (is_string($userpwd))
            curl_setopt($this->ch, CURLOPT_USERPWD, $userpwd);
    }

    public function postfields($postfields)
    {
        if (is_array($postfields) && !empty($postfields))
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
    }

    public function http_header($http_headers)
    {
        if (is_array($http_headers) && !empty($http_headers))
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $http_headers);
    }

    public function curl_get($get)
    {
        if (is_bool($get))
            curl_setopt($this->ch, CURLOPT_HTTPGET, $get);
    }

    public function curlexec()
    {
        $result = curl_exec($this->ch);
        if ($result === false)
            Logger::info("Curl Error: ".curl_error($this->ch));

        return $result;
    }

    public function __destruct()
    {
        $this->ch = false;
    }

    public function close()
    {
        curl_close($this->ch);
    }

    public function curl_timeout($timeout)
    {
        if (is_numeric($timeout))
            curl_setopt($this->ch, CURLOPT_TIMEOUT, $timeout);
    }

    public function failonError($fail)
    {
        if (is_bool($fail))
            curl_setopt($this->ch, CURLOPT_FAILONERROR, $fail);
    }

    public function followlocation($follow)
    {
        if(is_bool($follow))
            curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, $follow);
    }

    public function curlError()
    {
        return curl_error($this->ch);
    }

}
