<?php
namespace App\libraries;

use System\Library;
use System\Ini;
use App\libraries\memcacheLib as MemcacheLib;
use App\libraries\curl as Curl;
use System\Logger;
use App\entity\PlexEntity;

class PlexLib extends Library {

    //plex library keys
    const CH_MUSIC = 23;
    const KOR_MUSIC = 36;
    const JAP_MUSIC = 25;
    const ENG_MUSIC = 26;

    public $MediaServerIP;
    public $MediaServerUrl;
    public $plexUrl;
    private $headers;
    private $access_token;
    private $accountDetails;
    private $memcache;
    private $libraryKey;
    // private $plexClientIP = null;
    // private $plexClientPort = null;
    private $plexEntity;

    public function __construct(PlexEntity $plexEntity)
    {
        $this->plexEntity = $plexEntity;
        $this->MediaServerIP = Ini::getConfig("homeweb.HomePlex_ip");
        $this->MediaServerUrl = Ini::getConfig("homeweb.HomePlex_url");
        $this->plexUrl = Ini::getConfig("homeweb.Plex_url");
        $this->curl = new Curl;
        $this->memcache = new MemcacheLib;
        $this->access_token = $this->memcache->getItem("access_token");
        if (!$this->access_token) {
			$this->signInPlex();
        }
    }

	/**
	Playback MUST follow these rules.
	http://<CLIENT IP>:<CLIENT PORT>/player/playback/playMedia?key=%2Flibrary%2Fmetadata%2F<MEDIA ID>&offset=0&machineIdentifier=<SERVER ID>&address=<SERVER IP>&port=<SERVER PORT>&protocol=http&path=http%3A%2F%2F<SERVER IP>%3A<SERVER PORT>%2Flibrary%2Fmetadata%<MEDIA ID>

	Client = Device of where the content is to be played/actioned/streamed.
	Server = Plex server that holds the content.
	**/
    public function playMedia($client, $MediaKey)
    {
        Logger::info("playing media: ".$MediaKey);
		// $device = $this->getDevices("Nexus 5X");

		// $url = "http://192.168.1.23:32400/player/playback/playMedia";
		// $url = $device['connection'][0]."/player/playback/playMedia";
        $url = $client."/player/playback/playMedia";
		// $url .= "?key=/library/metadata/6740";
        $url .= "?key=".$MediaKey;
        $url .= "&machineIdentifier=d2ab63258beb5cd78ab7f494e886d8f654764ef0";
        $url .= "&address=".$this->MediaServerIP;
        $url .= "&port=32400";
        $url .= "&protocol=http";
		// $url .= "&path=http://192.168.1.11:37713".$MediaKey;
        // print_r($url);
        Logger::info("play url: ".$url);

        $this->headers = array("Content-type: application/xml");

        $this->curl->curl_url($url);
        $this->curl->returnTransfer(true);
        $this->curl->http_header($this->headers);
        $this->curl->curl_get(true);
        $result = $this->curl->curlexec();
		// $this->curl->close();
		
        $xml = new \SimpleXMLElement($result);
        // Logger::info("result: ".var_Export($result, true));
        $result = array("code" => (string)$xml->attributes()["code"], "message" => (string)$xml->attributes()['status']);
        // Logger::info("xml result: ".var_Export($newresult, true));
		// return $result;
        $this->plexEntity->setData($result);

    }

    public function searchMedia($query, $getVideo=true, $getDirectory=true)
    {
        $query = urlencode($query);

        $result = $this->commonGet($this->MediaServerUrl."search?query=".$query);
		// $this->curl->close();
        if (!$result) {
            throw new \Exception("Cannot query server");
        }
		
        $xml = new \SimpleXMLElement($result);

        $video = array();
        if (isset($xml->Video) && $getVideo) {
            $required = array("title", "sourceTitle", "type", "duration", "year", "key");
            $video = $this->buildReturnData($xml->Video, $required);
        }

        $directory = array();
        if (isset($xml->Directory) && $getDirectory) {
		// error_log("xml: ".var_export($data, true));
            $required = array("parentTitle", "title", "type", "key", "leafCount", "librarySectionTitle", "parentThumb");
            $directory = $this->buildReturnData($xml->Directory, $required);
        }

        $tracks = $this->searchTracks($query);
		
        $allvideos = array_merge($directory, $video);
        Logger::info("allvideos: ".var_Export($allvideos, true));

        $alldata = array_merge($allvideos, $tracks);
        Logger::info("alldata: ".var_Export($alldata, true));

        $this->plexEntity->setData($alldata);
        // return $alldata;
    }

    public function getServers()
    {
        $this->headers = array("X-Plex-Token: ".$this->access_token);
        $this->curl->curl_url($this->plexUrl."pms/servers.xml");
        $this->curl->returnTransfer(true);
        $this->curl->http_header($this->headers);
        $this->curl->curl_get(true);
        $result = $this->curl->curlexec();
		// $this->curl->close();

        if (!$result) {
			throw new \Exception("Cannot get servers");
        }

        $xml = new \SimpleXMLElement($result);
		
        $required = array("accessToken", "name", "address", "port", "scheme", "host", "localAddresses",
            "machineIdentifier", "version", "updatedAt");
        $data = $this->buildReturnData($xml->Server, $required);

        return $data;
    }

    public function getLibraries()
    {
        $playback_url = "library/sections";

        $result = $this->commonGet($this->MediaServerUrl.$playback_url);
        $this->curl->close();

        $xml = new \SimpleXMLElement($result);

        $libraries = array();
        $i = 0;
        foreach($xml->Directory AS $directory):
            $libraries[$i]['title'] 		= (string)$directory->attributes()['title'];
            $libraries[$i]['language'] 		= (string)$directory->attributes()['language'];
            $libraries[$i]['key'] 			= (string)$directory->attributes()['key'];
            $libraries[$i]['type'] 			= (string)$directory->attributes()['type'];
            $libraries[$i]['updatedAt']		= (string)$directory->attributes()['updatedAt'];
            $libraries[$i]['composite']		= (string)$directory->attributes()['composite'];
            $libraries[$i]['uuid']			= (string)$directory->attributes()['uuid'];
            $libraries[$i]['createdAt']		= (string)$directory->attributes()['createdAt'];
            $libraries[$i]['locationId']	= (string)$directory->Location->attributes()['id'];
            $libraries[$i]['locationPath']	= (string)$directory->Location->attributes()['path'];

            $i++;
        endforeach;
		
        $this->plexEntity->setData($libraries);
		
    }

    public function getDevices($getDevice=false)
    {
        Logger::info("getting devices");
		// $this->headers = array("X-Plex-Token: ".$this->access_token);
        $this->curl->curl_url($this->plexUrl."devices.xml");
        $this->curl->returnTransfer(true);
                // $this->curl->http_header($this->headers);
        $this->curl->curl_get(true);
        $result = $this->curl->curlexec();
                // $this->curl->close();

        if (!$result) {
            throw new \Exception("Cannot get list of devices");
        }

        $xml = new \SimpleXMLElement($result);
        $data = array();
        $i=0;
        foreach ($xml->Device as $device):
            $data[$i]['deviceName'] = (string)$device->attributes()['name'];
            $data[$i]['publicIP'] 	= (string)$device->attributes()['publicAddress'];
            $data[$i]['product'] 	= (string)$device->attributes()['product'];
            $data[$i]['platform'] 	= (string)$device->attributes()['platform'];
            $data[$i]['platformVersion'] = (string)$device->attributes()['platformVersion'];
            $data[$i]['clientId'] 	= (string)$device->attributes()['clientIdentifier'];
            $data[$i]['id'] 		= (string)$device->attributes()['id'];
            $data[$i]['device'] 	= (string)$device->attributes()['device'];
            $data[$i]['token'] 		= (string)$device->attributes()['token'];
            $data[$i]['lastSeen'] 	= (string)$device->attributes()['lastSeenAt'];
            $data[$i]['provides'] 	= (string)$device->attributes()['provides'];

            if ($device->Connection) {
                foreach ($device->Connection as $connection):
                    $data[$i]['connection'][] = (string)$connection->attributes()['uri'];
                endforeach;
            }
                        
            $i++;
        endforeach;
                
        $returnDevice = $data;

        if ($getDevice && !empty($data)){
            foreach ($data as $clientDevice):
            if (strpos($clientDevice['deviceName'], $getDevice)) {
                $returnDevice = $clientDevice;
                break;
            }

            endforeach;
        }

        return $returnDevice;

    }

    public function signInPlex()
    {
        Logger::info("signing in");
        $server_details = $this->getPlexServerDetails();
        // Logger::info("servers: ".var_Export($server_details, true));

        $this->headers = array(
            "Content-type: application/json", 
            "Authorization: Basic ".base64_encode(Ini::getConfig("homeweb.global_username").
            ":".Ini::getConfig("homeweb.password")),
            "X-Plex-Platform: ". $server_details['platform'],
            "X-Plex-Platform-Version: " . $server_details['platformVersion'],
            "X-Plex-Version: " . $server_details['version'],
            "X-Plex-Client-Identifier: ".$server_details['machineIdentifier']
        );
        // Logger::info("headers: ".var_export($this->headers, true));

        $this->curl->curl_url($this->plexUrl."users/sign_in.json");
        $this->curl->returnTransfer(true);
        $this->curl->http_header($this->headers);
        $this->curl->curl_post(true);
        $result = $this->curl->curlexec();

        $data = json_decode($result);	
        // Logger::info("result: ".var_export($data, true));
        if (!$data || isset($data->error)) {
                    // throw new Exception("Cannot sign in");
            Logger::error("Cannot sign in");
            return false;
        }
		
        $this->access_token = $data->user->authentication_token;
        $this->memcache->addItem("access_token", $this->access_token, 2400);
        Logger::info("got access token");

    }

    public function getAllMediaInLibrary($libraryId)
    {
        // Logger::info("key: ".$this->libraryKey);
        $playback_url = ($this->libraryKey)
            ? "library/sections/".$this->libraryKey."/all"
            : "library/sections/".$libraryId."/all";


        $result = $this->commonGet($this->MediaServerUrl.$playback_url);
		// $this->curl->close();

		if (!$result) {
			throw new \Exception("Cannot get media in Library: ".$libraryId);
        }

		$xml = new \SimpleXMLElement($result);
		$required = array("key", "studio", "type", "title", "originalTitle", "thumb", 
							"summary", "year", "duration", "originallyAvailableAt");

        $data = array();
        if (isset($xml->Video)) {
            $data = $this->buildReturnData($xml->Video, $required);
        }

        if (isset($xml->Directory)) {
            $data = $this->buildReturnData($xml->Directory, $required);
        }
        // Logger::info("mediaInLibrary: ".var_Export($data, true));
		$this->plexEntity->setData($data);
	}

	public function getPlexServerDetails()
	{
		// $this->headers = array("Content-type:application/xml");
        Logger::info("getting plex server details");
		$this->curl->curl_url($this->MediaServerUrl);
		// $this->curl->curl_url($this->plexUrl."pms/servers");
		// $this->curl->curl_url($this->plexUrl."devices.xml");
		$this->curl->returnTransfer(true);
		// $this->curl->http_header($this->headers);
		$result = $this->curl->curlexec();

		if(!$result)
			throw new \Exception("Cannot get plex server details");

		$xml = new \SimpleXMLElement($result);
        Logger::info("details: ".var_export($xml, true));

		// $required = array("myPlexUsername", "platform", "platformVersion", 
		$required = array("platform", "platformVersion", 
							"version", "friendlyName", "machineIdentifier");

		foreach($required AS $require):
			$this->accountDetails[$require] = (string)$xml->attributes()[$require];
		endforeach;

		return $this->accountDetails;

	}

	private function buildReturnData($xml, $required)
	{
		$i=0;
		$result = array();
		foreach($xml AS $data):
			foreach($required AS $require):
                // if (isset($data->attributes()[$require]))
                if ($require == "thumb")
                    $result[$i][$require] = (string)$data->attributes()[$require]."?X-Plex-Token=".$this->access_token;
                else
                    $result[$i][$require] = (string)$data->attributes()[$require];
                
                if (isset($data->Media->Part)){
                    $result[$i]['MediaPart'] = (array)$data->Media->Part->attributes();
                    $result[$i]['Token'] = "?X-Plex-Token=".$this->access_token;
                }
			endforeach;
			$i++;
		endforeach;

		return $result;
	}


    public function getRecentlyAdded($items=10, $movie=true, $series=true)
    {
        $url = $this->MediaServerUrl."library/recentlyAdded?X-Plex-Container-Size=".$items."&X-Plex-Container-Start=0";
        Logger::info("url: ".$url);
		$result = $this->commonGet($url);
    
        $xml = new \SimpleXMLElement($result);
        Logger::info("results: ".$xml);
        // Logger::info("plex data: ".var_export($xml, true));
        if (!isset($xml->Directory)) {
            return false;
        }

        $required = array("parentTitle", "title", "leafCount", "librarySectionTitle", "parentThumb", "type", "index");
        $directory = array();
        if (isset($xml->Directory) && $series) {
            $directory = $this->buildReturnData($xml->Directory, $required);
        }

        $required = array("title", "year", "thumb");
        $video = array();
        if (isset($xml->Video) && $movie) {
            $video = $this->buildReturnData($xml->Video, $required);
        }

        $this->plexEntity->setData(array_merge($directory, $video));
        // return $this->buildData($xml->Directory, $required);
    }

    public function getRecentAddedMusic($items=10)
    {
        $url = $this->MediaServerUrl."library/sections";
        // Logger::info("url: ".$url);
		$result = $this->commonGet($url);
    
        $xml = new \SimpleXMLElement($result);
        // Logger::info("results: ".$xml);
        // Logger::info("plex data: ".var_export($xml, true));
        if (!isset($xml->Directory)) {
            return false;
        }

        $sections = [];
        foreach ($xml->Directory as $dir) {
            if ($dir->attributes()['type'] == "artist") {
                $sections[] = [
                    "key" => $dir->attributes()['key'],
                    "art" => $dir->attributes()['art'],
                    "title" => $dir->attributes()['title'],
                    "thumb" => $dir->attributes()['thumb'],
                    "type" => $dir->attributes()['type'],
                    "composite" => $dir->attributes()['composite'],
                ];
            }
        }
        // Logger::info("sections: ".var_export($sections, true));

        if (empty($sections)) {
            return $sections;
        }

        $recentTracks = [];
        foreach ($sections as $section) {
            $url  = $this->MediaServerUrl."library/sections/".$section['key'].
                    "/recentlyAdded?X-Plex-Container-Size=".$items."&X-Plex-Container-Start=0";
            $result = $this->commonGet($url);
            $xml = new \SimpleXMLElement($result);
            $required = array("key", "title", "type", "parentKey", "leafCount", "thumb", "addedAt", "parentTitle");

            $recentTracks[] = $this->buildReturnData($xml->Directory, $required);
        }
        Logger::info("recent: ".var_Export($recentTracks, true));

        if (empty($recentTracks)) {
            return $recentTracks;
        }

        $tracks = [];
        foreach ($recentTracks as $library) :
            foreach ($library as $album):
                $tracks[] = $this->getAlbumSongs($album['key'], false); 
            endforeach;
        endforeach;

        $this->plexEntity->setData($tracks);
    }

    public function getAllEpisodes($libraryId)
    {
        $url = rtrim($this->MediaServerUrl, "/").$libraryId;

        $result = $this->commonGet($url);

        $xml = new \SimpleXMLElement($result);
        // Logger::info("xml: ".var_Export($xml, true));

        if (!isset($xml->Directory)) {
            return false;
        }

        $required = array("key", "title", "type", "parentKey", "leafCount", "thumb", "addedAt");

        $seasons = $this->buildReturnData($xml->Directory, $required);

        if (empty($seasons)) {
            return false;
        }
        // $season['parentTitle'] = (string)$xml->attributes()[$require];
        // Logger::info("xml: ".var_Export($xml, true));

        //Season Parent Title
        $seasons[0]['parentTitle'] = (string)$xml->attributes()['parentTitle'];
        // Logger::info("seasons: ".var_export($seasons, true));

        foreach($seasons AS &$season):
            $list = $this->getSeasonEpisodes($season['key']);
            if ($list) {
                $season['episodelist'] = $list;
            }
        endforeach;
        // Logger::info("seasons: ".var_export($seasons, true));
        $this->plexEntity->setData($seasons);
    }

    public function getSeasonEpisodes($key)
    {
        // Logger::info("getting episodes: ".$key);
        $url = rtrim($this->MediaServerUrl, "/").$key;

        $result = $this->commonGet($url);

        $xml = new \SimpleXMLElement($result);
        // Logger::info("episodes: ".var_export($xml, true));

        if (!isset($xml->Video)) {
            return false;
        }

        Logger::info("building data");
        // Logger::info("eps: ".var_Export($xml, true));
        $required = array("key", "title", "type", "year", "thumb", "duration", "addedAt", "viewCount", "index");
        $episodes = array();
        $episodes = $this->buildReturnData($xml->Video, $required);
        // Logger::info("episodes2: ".var_export($episodes, true));

        return $episodes;
    }

    public function OnDeck()
    {
        Logger::info("getting on deck");
        $items = 10;

        $url = $this->MediaServerUrl."library/onDeck?X-Plex-Container-Size=".
                $items."&X-Plex-Container-Start=0";

		$result = $this->commonGet($url);
    
        $xml = new \SimpleXMLElement($result);

        if (!isset($xml->Video)) {
            return false;
        }

        Logger::info("building on deck data");
        $required = array("key", "title", "type", "year", 
                            "thumb", "duration", "addedAt", "grandparentTitle");
        $video = $this->buildReturnData($xml->Video, $required);
        // Logger::info("episodes2: ".var_export($episodes, true));
        $this->plexEntity->setData($video);
    }

    public function getAvailableClients($homeTheatre=false)
    {
        $url = $this->MediaServerUrl."clients";

		$result = $this->commonGet($url);

        $xml = new \SimpleXMLElement($result);

        // Logger::info("clients: ".var_export($xml, true));
        if (!isset($xml->Server)) {
            return false;
        }

        $required = array("name", "host", "protocol", "port", 
                            "product", "deviceClass", "protocolCapabilities");
        $clients = $this->buildReturnData($xml->Server, $required);

        if ($homeTheatre){
            foreach ($clients as $client) :
                if ($client['product'] == "plexhometheater"){
                    return $client;
                }
            endforeach;
        }

        $this->plexEntity->setClients($clients);

    }

    public function getSongList($mediaKey)
    {
        $url = rtrim($this->MediaServerUrl, "/").$mediaKey;
        Logger::info("song list url: ".$url);

        $result = $this->commonGet($url);

        $xml = new \SimpleXMLElement($result);

        if (!isset($xml->Directory)) {
            return false;
        }

        $required = array("key", "title", "type", "parentKey", "leafCount", "thumb", "addedAt", "grandParentTitle");
        // Logger::info("building albums");

        $albums = $this->buildReturnData($xml->Directory, $required);
        // Logger::info("albums: ".var_export($albums, true));

        if (empty($albums)) {
            return false;
        }

        $albums[0]['parentTitle'] = (string)$xml->attributes()['parentTitle'];

        foreach ($albums as &$album) {
            $tracks = $this->getAlbumSongs($album['key'], false);
            if ($tracks) {
                $album['tracklist'] = $tracks;
            }
        }
        // Logger::info("seasons: ".var_export($seasons, true));
        $this->plexEntity->setData($albums);
    }

    public function getAlbumSongs($key, $artist=true)
    {
        $url = rtrim($this->MediaServerUrl, "/").$key;

        $result = $this->commonGet($url);

        $xml = new \SimpleXMLElement($result);
        // Logger::info("tracks: ".var_Export($xml, true));

        if (!isset($xml->Track)) {
            return false;
        }

        // Logger::info("eps: ".var_Export($xml, true));
        $required = array("key", "title", "type", "year", "thumb", 
                            "duration", "addedAt", "parentTitle", "grandparentTitle");
        $tracks = array();
        $tracks = $this->buildReturnData($xml->Track, $required);

        // Logger::info("tracks: ".var_export($albumTracks, true));
        if ($artist) {
            $albumTracks = array("title" => $tracks[0]['parentTitle'], "tracklist" => $tracks);
            // return $tracks;
            return $albumTracks;
        } 

        return $tracks;

    }

    public function getPlaylist() 
    {
        Logger::info("playlist url: ".$this->MediaServerUrl."playlists");
        $result = $this->commonGet($this->MediaServerUrl."playlists");
        Logger::info("playlist result: ".var_export($result, true));

        if (!$result) {
            throw new \Exception("Cannot query server");
        }

        $xml = new \SimpleXMLElement($result);

        $playlist = array();

        if (isset($xml->Playlist)) {
            $required = array("key", "title", "type", "addedAt");
            $playlist = $this->buildReturnData($xml->Playlist, $required);
            // Logger::info("tracks: ".var_export($tracks, true));
        }

        return $playlist;
    }

    private function searchTracks($query)
    {
		// $query = urlencode($query);
        Logger::info("url: ".$this->MediaServerUrl."search?query=".$query."&type=10");
        $result = $this->commonGet($this->MediaServerUrl."search?query=".$query."&type=10");
        // print_r($this->MediaServerUrl."search?query=".$query."&type=10");
        Logger::info("tracks: ".var_export($result, true));

        if (!$result) {
            throw new \Exception("Cannot query server");
        }

        $xml = new \SimpleXMLElement($result);
        // Logger::info("alltracks: ".var_export($xml, true));

        $tracks = array();
        if (isset($xml->Track)) {
            $required = array("key", "title", "type", "year", "thumb", "duration", "addedAt", "parentTitle");
            $tracks = $this->buildReturnData($xml->Track, $required);
            // Logger::info("tracks: ".var_export($tracks, true));
        }

        return $tracks;
    }

    public function searchShow($query)
    {
        // $result = $this->commonGet("https://plex.tv/pms/playlists/queue/unwatched");
        // error_log("res: ".var_export($result, true));

        // print_R($result);
    }

    private function commonGet($url)
    {
        $this->headers = array(
            "Content-type: application/xml",
            "X-Plex-Token: ".$this->access_token
        );
        $this->curl->curl_url($url);
        $this->curl->returnTransfer(true);
        $this->curl->http_header($this->headers);
        $this->curl->curl_get(true);
        return $this->curl->curlexec();
    }

    public function availableLibraryMusic()
    {
        return array(
            "chinese" => self::CH_MUSIC, 
            "japanese" => self::JAP_MUSIC, 
            "korean" => self::KOR_MUSIC,
            "english" => self::ENG_MUSIC
        );
    }


    public function selectMusicLibrary(int $libraryKey)
    {
        $this->libraryKey = $libraryKey;
    }


}
