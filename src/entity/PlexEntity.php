<?php
namespace App\entity;

use System\BaseController;
use System\Response;
use System\Request;

class PlexEntity
{

    private $result = array();
    private $request;

    public function __construct()
    {
        $this->request = (new Request)->getRequest();
    }

    public function getKeyword()
    {
        // error_log("request: ".var_export($_POST, true));
        // return BaseController::filterVar($this->request['keyword']);
        // return $this->request->get("keyword");
        return $this->request->post("keyword");
    }

    public function getId()
    {
        // return BaseController::filterVar($this->request['id']);
        return $this->request->post('id');
    }

    public function getMediaKey()
    {
        // return BaseController::filterVar($this->request['mediaKey']);
        return $this->request->post('mediaKey');
    }

    public function getClient()
    {
        // return BaseController::filterVar($this->request['client']);
        return $this->request->post('client');
    }

    public function getLang()
    {
        // return BaseController::filterVar($this->request['lang']);
        return $this->request->post('lang');
    }

    public function getTitle()
    {
        // return BaseController::filterVar($this->request['title']);
        return $this->request->post('title');
    }

    public function getSeason()
    {
        // return BaseController::filterVar($this->request['season']);
        return $this->request->post('season');
    }

    public function getEpisode()
    {
        // return BaseController::filterVar($this->request['episode']);
        return $this->request->post('episode');
    }

    public function getControl()
    {
        // return BaseController::filterVar($this->request['control']);
        return $this->request->post('control');
    }

    public function getCategory()
    {
        // return BaseController::filterVar($this->request['category']);
        return $this->request->post('category');
    }

    public function getClients()
    {
        $this->result['code'] = (!empty($this->result['clients'])) 
            ? Response::SUCCESS
            : Response::NO_CONTENT;
        return $this->result;
    }

    public function setData($data)
    {
        $this->result['data'] = $data;
    }

    public function setClients($clients)
    {
        $this->result['clients'] = $clients;
    }

    public function getData()
    {
        return $this->result['data'];
    }

    public function getResult()
    {
        $this->result['code'] = (!empty($this->result['data'])) 
            ? Response::SUCCESS
            : Response::NO_CONTENT;

        return $this->result;

    }


}
